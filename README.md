# aws-jwt-auth

Following the guide from https://authress.io/knowledge-base/aws-gitlab-cicd-login-authentication

## Goal

The goal of this project is to give a simple way to test if a GitLab CI Job can assume a role using `$CI_JOB_JWT_V2`.

**Steps**
1. Ensure you have an IAM Policy with `sts:AssumeRole`, and set it's arn to `assume_role_arn`
   Example:
   ```json
    {
        "Version": "2012-10-17",
        "Statement": {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::999999999999:role/Test*"
        }
    }
   ```
2. Add a `terraform.tfvars` file with your gitlab_url, aud value, and the role to assume.
    This Example would allow any project in the `flightjs` group running a job for the `main` branch. 
    ```
    gitlab_url      = "https://gitlab.com"
    aud_value       = "https://gitlab.com"
    match_field     = "sub"
    match_value     = ["project_path:flightjs/*:ref_type:branch:ref:main"]
    assume_role_arn = ["arn:aws:iam::999999999999:policy/STS_Assume_Test"]
    ```

3.  Run terraform init and apply
4.  Set the ROLE_ARN variable in the GitLab Project (Settings->CI)
5.  Run the pipeline